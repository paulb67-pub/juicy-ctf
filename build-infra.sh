#!/bin/bash
set -x
EPOCH=`date +%s`
CTFKEY=`echo ${EPOCH} | md5sum | cut -d' ' -f1`
XMLFILE="OWASP-Juice-Shop-RTB-${EPOCH}.xml"

# Config Generator
sed -i "s/ctfKey: .*/ctfKey: ${CTFKEY}/" juice-shop-config.yaml
npm install -g juice-shop-ctf-cli
juice-shop-ctf --config juice-shop-config.yaml --output ${XMLFILE}

# Get and Config multi-juicer
if [ ! -d "multi-juicer" ]; then
	git clone https://github.com/iteratec/multi-juicer.git
fi
cp multi-juicer/helm/multi-juicer/values.yaml .
sed -i "s/ctfKey: .*/ctfKey: \"${CTFKEY}\"/" values.yaml
sed -i "s/nodeEnv: .multi-juicer./nodeEnv: \"ctf\"/" values.yaml

export TF_VAR_project_id=${GOOGLE_CLOUD_PROJECT}
terraform plan

exit
# Game Server (RTB)
  #GCE VM e2.small
  #  rtb-vm-init.sh
  #Firewall -> 8888
  #Connect xxx:8888
  #  Reset admin password
  #  Restore from OWASP....xml
# GKE Autopilot Cluster

# CREATE CLUSTER TF or CLI
#tf apply

kubectl cluster-info

helm install -f values.yaml multi-juicer ./multi-juicer/helm/multi-juicer/

# retrieve the admin password
BAL_ADMIN_PASS=`kubectl get secrets juice-balancer-secret -o=jsonpath='{.data.adminPassword}' | base64 --decode`

wget https://raw.githubusercontent.com/iteratec/multi-juicer/master/guides/k8s/k8s-juice-service.yaml
kubectl apply -f k8s-juice-service.yaml
# now call kubectl describe until the public ip address is shown
sleep 10
kubectl describe svc multi-juicer-loadbalancer

echo "Juice Balancer Admin Password : ${BAL_ADMIN_PASS}"
